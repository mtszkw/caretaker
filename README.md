# caretaker

### Purpose
Do you sometimes fall asleep when watching movies in the late night? Or maybe you listen to music when falling asleep?  
If you do so and you don't want your computer to run till you wake up in the morning, use Caretaker to schedule computer shutdown when feeling sleepy. It takes only few clicks and you can sleep tight.

No need to remember about turning your computer off when sleepy.  
No need to type long command lines and calculate time in seconds (as in Windows).  
If your device is far away (e.g. connected with TV via HDMI) you'd only need wireless mouse, few clicks and that's it.

### Usage
Grab the latest version of Caretaker from *build/* directory above.  
All you need to do is run the executable (no dependencies needed) and choose appropriate duration using buttons. Then press **Schedule shutdown** button and good night. You can still abort scheduled shutdown using **Cancel** button. It'll also reset the timer and allow you to set it once again.

### Screenshots

![Caretaker](https://image.ibb.co/bYwqJH/caretaker.png)
